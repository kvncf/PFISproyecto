{-# LANGUAGE PackageImports #-}
import "PFISKvn" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
